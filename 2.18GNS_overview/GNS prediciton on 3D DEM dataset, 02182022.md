# GNS prediciton on 3D DEM dataset, 02/18/2022


## 1. Design of experiments for the 3D DEM dataset


| Experiment No | Connectivity radius | Message passing steps | Noise standard deviation | Edge embedding length | Node embedding length | size of MLP | sampling frequency from DEM datasets | Loss function | Particle-wise Mean Squared Error |
| - | - | - | - | - | - | - | - | - | - |
|1	|0.01	|5	|0.0001	|3	|8	|32	|1	|L2||
|2	|0.05	|5	|0.0001	|3	|8	|128	|40	|L1||
|3	|0.01	|15	|0.0001	|3	|21	|32	|40	|L1||
|4	|0.05	|15	|0.0001	|3	|21    |128	|1	|L2||
|5	|0.01	|5	|0.001	|3	|21	|128	|40	|L2||
|6	|0.05	|5	|0.001	|3	|21	|32	|1	|L1||
|7	|0.01	|15	|0.001	|3	|8	|128	|1	|L1||
|8	|0.05	|15	|0.001	|3	|8	|32	|40	|L2||
|9	|0.01	|5	|0.0001	|16	|21	|128	|1	|L1||
|10	|0.05	|5	|0.0001	|16	|21	|32	|40	|L2||
|11	|0.01	|15	|0.0001	|16	|8	|128	|40	|L2||
|12	|0.05	|15	|0.0001	|16	|8	|32	|1	|L1||
|13	|0.01	|5	|0.001	|16	|8	|32	|40	|L1||
|14	|0.05	|5	|0.001	|16	|8	|128	|1	|L2||
|15	|0.01	|15	|0.001	|16	|21	|32	|1	|L2||
|16	|0.05	|15	|0.001	|16	|21	|128	|40	|L1||
|17	|0.03	|12	|0.0003	|9	|21	|128	|10	|L1||
|18	|0.03	|12	|0.0003	|16	|21 |32	|40	|L1||
|19	|0.03	|12	|0.0003	|16	|21	|128	|20	|L1||
|20	|0.03	|12	|0.0003	|9	|21	|128	|5	|L2||


## 2.  GNS implementation on 3D DEM dataset


### 2.1.  3D dataset baseline currently
- connectivity radius = 0.025
- message passing steps = 12
- noise_std = 3e-4
- mean and std of velocity, acceleration is calculated based on the given trajectories, so velocity and acceleration is normalized during training and infering
- Edge embedding length = 16
- Node embedding length = 21

|animation|
|:-:|
|![animation](./3D_baseline2/GIF 2022-2-16 15-50-16.gif)|

|loss plot|
|:-:|
|![lossplot](./3D_baseline2/Screenshot_1.png)|