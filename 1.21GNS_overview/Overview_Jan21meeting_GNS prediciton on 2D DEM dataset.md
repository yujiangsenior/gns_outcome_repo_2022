# GNS prediciton on 2D DEM dataset, 01/21/2022
### Details of current GNS framework
![result](./gns_diagram_v2.png)
### 1. Baseline currently 
- connectivity radius = 0.015
- message passing steps = 12 
- noise_std = 3e-4
- mean and std of velocity, acceleration is calculated based on the given trajectories, so velocity and acceleration is normalized during training and infering

| lossplot  | gif |
| :-------------: | :-------------: |
| ![loss](./loss/Screenshot_loss_baseline2.png)  | ![gif](./baseline_2_rollouts/1000_3region_cohesion_meanstd_adjust_test2_rollout_3.gif)  |



### 2. On behalf of Baseline, adding the edge embedding with overlaps between particles
- Edge embedding length = 4 (it was 3)

| lossplot  | gif |
| :-------------: | :-------------: |
|![loss](./loss/Screenshot_loss_edgeembed_overlap.png)|![gif for edge embedding length equals 4](./baseline_2_edgeembed_overlap/1000_3region_cohesion_meanstd_adjusted_edgeembed_overlap2_rollout_3.gif)|


### 3. On behalf of Baseline, adding the  edge embeddings with relative velocity among particles 
- Edge embedding length = 5 (it was 3)

| lossplot  | gif |
| :-------------: | :-------------: |
|![loss](./loss/Screenshot_loss_edgeembed_v.png)|![gif for edge embedding length equals 5](./baseline_2_edgeembed_v/meanstd_adjust_edgeembed_relative_v0_rollout_3.gif)|


### 4. On behalf of Baseline, changing the input sequence length to 3
- Input sequence length = 3 (it was 6)

| lossplot  | gif |
| :-------------: | :-------------: |
|![loss](./loss/Screenshot_loss_inputlen3.png)|![gif for](./baseline_2_input3/1000_3region_cohesion_meanstd_adjust_input3_1_rollout_3.gif)|

### 5. On behalf of 2, 3, change edge embedding with relative velocity and overlaps between particles
- Edge embedding length = 6 (it was 3)

| lossplot  | gif |
| :-------------: | :-------------: |
|![loss](./loss/Screenshot_loss_edgeembed6.png)|![gif for edge embedding length equals 6](./baseline_2_edgeembed6/baseline_2_edgeembed6_rollout_3.gif)|
