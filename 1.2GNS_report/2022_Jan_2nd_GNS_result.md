# 2022 Jan 2nd, GNS result
## Details of current GNS framework
![result](./gns_diagram_v2.png)
## 1. Changing the length of node embedding from 30 to 16, by changing 16 to 2 in terms of the particle_type_embedding_size
- Other parameters
	- connectivity radius = 0.015
	- message passing steps = 12

- Comparison

|particle type embedding size|log loss plot|animation|
|:--:|:--:|:--:|
|16|![loss](./loss/logloss_15th_1000_3region_cohesion_r0015mp12_2ndnoise.png)|![16](./gif/15th_test_1000_3region_cohesion_r0015mp12_2ndnoise_rollout_test_3.gif)|
|2|![loss](./loss/logloss_15th_1000_3region_cohesion_embedding2.png)|![2](./gif/15th_test_1000_3region_cohesion_pt_embedding2_rollout_test_3.gif)|


## 2. Changing the edge embedding from 3 to 5, by adding relative velocity
- Other parameters
	- connectivity radius = 0.015
	- message passing steps = 12
- Comparison 

|edge embedding size|log loss plot|animation|
|:--:|:--:|:--:|
|3| ![loss](./loss/logloss_15th_1000_3region_cohesion_r0015mp12_2ndnoise.png)|![3](./gif/15th_test_1000_3region_cohesion_r0015mp12_2ndnoise_rollout_test_3.gif)|
|5|![loss](./loss/logloss_15th_1000_3region_cohesion_embedding_v.png)|![5](./gif/15th_test_1000_3region_cohesion_embedding_relative_v_rollout_test_3.gif)|
|5, particle_type_embedding_size: 2|![loss](./loss/logloss_15th_1000_3region_cohesion_pt2_edgeembedding_relative_v.png)|![2,5](./gif/15th_test_1000trajectory_3region_cohesion_pt2_edgeembedding_relative_v_rollout_test_3.gif)|

## 3. Changing the vel, acc mean/std in metadata.json

|vel, acc adjusted|log loss plot|animation|
|:--:|:--:|:--:|
|No|![loss](./loss/logloss_15th_1000_3region_cohesion_r0015mp12_2ndnoise.png)|![No](./gif/15th_test_1000_3region_cohesion_r0015mp12_2ndnoise_rollout_test_3.gif)|
|Yes|![loss](./loss/logloss_15th_1000_3region_cohesion_meanstdadjust.png)|![Yes](./gif/15th_test_1000_3region_cohesion_mean_std_adjusted_rollout_test_3.gif)|
|Yes, particle_type_embedding_size: 2|![loss](./loss/logloss_15th_1000_3region_cohesion_embedding2_meanstdadjusted.png)|![Yes](./gif/15th_test_1000_3region_cohesion_mean_std_adjusted_embedding2_rollout_test_3.gif)|
|Yes, edge_embedding size: 5|![loss](./loss/logloss_15th_1000_3region_cohesion_edgeembedding_relative_v_meanstd_adjusted.png)|![Yes](./gif/15th_test_1000trajectory_3region_cohesion_edgeembed_relative_v_meanstd_adjusted_rollout_test_3.gif)|
