# GNS prediciton on 2D DEM dataset, 02/04/2022

### 1. Baseline currently
- connectivity radius = 0.015
- message passing steps = 12 
- noise_std = 3e-4
- mean and std of velocity, acceleration is calculated based on the given trajectories, so velocity and acceleration is normalized during training and infering
- Edge embedding length = 12
- Node embedding length = 14

| lossplot  | gif |
| :-------------: | :-------------: |
|![loss](./baseline_3_node14edge12/nodefeature14_edge12_2.png)|![gif](./baseline_3_node14edge12/1000_3region_cohesion_meanstd_adjust_nodefeature14_edge12_3_rollout_3.gif)|
|![gif](./baseline_3_node14edge12/1000_3region_cohesion_meanstd_adjust_nodefeature14_edge12_3_rollout_0.gif)|![gif](./baseline_3_node14edge12/1000_3region_cohesion_meanstd_adjust_nodefeature14_edge12_3_rollout_2.gif)|
|![gif](./baseline_3_node14edge12/1000_3region_cohesion_meanstd_adjust_nodefeature14_edge12_3_rollout_1.gif)|![gif](./baseline_3_node14edge12/1000_3region_cohesion_meanstd_adjust_nodefeature14_edge12_3_rollout_4.gif)|


### 2. On behalf of baseline, changing node embedding
- Node embedding length =24

| lossplot  | gif |
| :-------------: | :-------------: |
|![loss](./baseline_2_edge12input3/Screenshot_edge12_input3_5.png)|![gif](./baseline_2_edge12input3/bessemer_data_rollouts_meanstd_adjust_edgeembed12_input3_5_rollout_3.gif)|



### 3. On behalf of baseline, changing Processor() part with new Graph Convolution Network (GCN)

| lossplot  | gif |
| :-------------: | :-------------: |
|![loss](./baseline_3_gcn/Screenshot_baseline3_gcn.png)|![gif](./baseline_3_gcn/bessemer_data_rollouts_meanstd_adjust_edge12node14_gcn_0_rollout_3.gif)|

### 4. On behalf of baseline,  also implement GNS on 3D DEM dataset

| lossplot  | gif |
| :-------------: | :-------------: |
|![loss](./3D_baseline/Screenshot_3D_baseline.png)|![gif](./3D_baseline/meanstd_adjust_edge16node21_4_rollout_0.gif)|

### 5. On behalf of baseline, change to another dataset that record the particle system for every step of DEM simulation (output frequency: dt = 0.0005s, previously it was dt = 0.0025s)

| sample per 20 steps  | sample per 10 steps |
| :-------------: | :-------------: |
|![gif](./baseline_3_Sample20/sample_200_rollout_3.gif)|![gif](./baseline_3_Sample10/1000_3region_cohesion_meanstd_node14edge12_100traj_dt5e-4_sample10_0_rollout_3.gif)|
|![gif](./baseline_3_Sample20/sample_200_rollout_1.gif)|![gif](./baseline_3_Sample10/1000_3region_cohesion_meanstd_node14edge12_100traj_dt5e-4_sample10_0_rollout_1.gif)|
