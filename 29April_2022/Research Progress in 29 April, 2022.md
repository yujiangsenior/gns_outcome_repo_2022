# Outlook for meeting on PhD research Progress in 29 April, 2022

## 1. GNS implementation on a new quasi-2D Angle of Repose dataset (500 trajectories)
- **sampling rate = 10**
- connectivity_radius = 0.03
- noise_std = 1e-3
- message passing steps = 5
- input sequence length = 6
- edge embedding length = 16
- mlp size = 32
- loss function: L1 loss
- training stops after 12,000 steps

|Loss curve on validation dataset|loss curve on training dataset|
|:---:|:---:|
|![curve1](./Screenshot_1.png)|![curve2](./Screenshot_2.png)|


|Testing Trajectory No.|animation|MSE|
|:-:|:-:|:-:|
|1|![animation](./rollout_0.gif)|0.00008|
|2|![animation](./rollout_1.gif)|0.00019|

## 2. Screening DoE of GNS implementation on the SandRamps (2D) dataset
![DoE](./Screening_DoE.png)
|Testing Trajectory No.|animation|MSE|
|:-:|:-:|:-:|
|1|![animation](./SandRamps_0.gif)|0.026|