# Outlook for meeting on PhD research Progress in 1st, April, 2022

## 1. GNS implementation on the new quasi-2D Angle of Repose dataset (100 trajectories), taking 4 parameters in the *DoE of GNS implementation on 3D DEM dataset*
| Experiment No | Connectivity radius | Message passing steps | *Noise standard deviation* | *Edge embedding length* | *Node embedding length* | size of MLP | *sampling frequency from DEM datasets* | Loss function | Particle-wise Mean Squared Error |
| - | - | - | - | - | - | - | - | - | - |
|1	|0.01	|5	|**0.0001**	|**4**	|**15**	|32	|**1**	|L2||
|2	|0.05	|5	|**0.0001**	|**4**	|**15**	|128	|**20**	|L1||
|3	|0.01	|15	|**0.0006**	|**4**	|**24**	|32	|**20**	|L1||
|4	|0.05	|15	|**0.0006**	|**4**	|**24**    |128	|**1**	|L2||
|5	|0.01	|5	|**0.001**	|**12**	|**24**	|128	|**10**	|L2||
|6	|0.05	|5	|**0.001**	|**12**	|**24**	|32	|**1**	|L1||
|7	|0.01	|15	|**0.0003**	|**12**	|**15**	|128	|**1**	|L1||
|8	|0.05	|15	|**0.0003**	|**12**	|**15**	|32	|**10**	|L2||
|9	|0.01	|5	|**0.00001**	|**13**	|**21**	|128	|**5**	|L1||
|10	|0.05	|5	|**0.00001**	|**13**	|**21**	|32	|**20**	|L2||
|11	|0.01	|15	|**0.0001**	|**13**	|**18**	|128	|**20**	|L2||
|12	|0.05	|15	|**0.0001**	|**13**	|**18**	|32	|**5**	|L1||
|13	|0.01	|5	|**0.001**	|**15**	|**18**	|32	|**20**	|L1||
|14	|0.05	|5	|**0.001**	|**15**	|**18**	|128	|**5**	|L2||
|15	|0.01	|15	|**0.002**	|**15**	|**21**	|32	|**5**	|L2||
|16	|0.05	|15	|**0.002**	|**15**	|**21**	|128	|**20**	|L1||
|17	|0.03	|12	|**0.0003**	|**4**	|**21**	|128	|**10**	|L1||
|18	|0.03	|12	|**0.0003**	|**12**	|**21** |32	|**5**	|L1||
|19	|0.03	|12	|**0.0006**	|**13**	|**21**	|128	|**20**	|L1||
|20	|0.03	|12	|**0.0006**	|**15**	|**21**	|128	|**1**	|L2||

### 1.1 Investigation of different input length, which also means different **node embedding length**
- Conclusion: input length 4  are better than others
- 
|Experimental No.|input length(node embedding length)|animation|MSE|
|:-:|:-:|:-:|:-:|
|1|3(15)|![animation](./input_len/3D_AoR_new2_3D_AoR_new_baseline4_3_1_rollout_0.gif)|0.00078|
|2|4(18)|![animation](./input_len/3D_AoR_new2_3D_AoR_new_baseline4_4_1_rollout_0.gif)|0.00063|
|3|5(21)|![animation](./input_len/3D_AoR_new2_3D_AoR_new_baseline4_5_1_rollout_0.gif)|0.001|
|4|6(24)|![animation](./input_len/3D_AoR_new2_3D_AoR_new_baseline4_6_1_rollout_0.gif)|0.0484|

### 1.2 Investigation of different noise standard deviation

- Conclusion: 1e-3 and 2e-3 are better than others

|Experimental No.|noise standard deviation|animation|MSE|
|:-:|:-:|:-:|:-:|
|1|5e-5|![animation](./noise_std/3D_AoR_new2_3D_AoR_new_baseline4_noisestd_5e-05_1_rollout_0.gif)|0.0289|
|2|1e-4|![ani](./noise_std/3D_AoR_new2_3D_AoR_new_baseline4_noisestd_0.0001_1_rollout_0.gif)|0.5524|
|3|3e-4|![ani](./noise_std/3D_AoR_new2_3D_AoR_new_baseline4_noisestd_0.0003_1_rollout_0.gif)|15.2118|
|4|6e-4|![ani](./noise_std/3D_AoR_new2_3D_AoR_new_baseline4_noisestd_0.0006_1_rollout_0.gif)|0.0039|
|5|1e-3|![animation](./noise_std/3D_AoR_new2_3D_AoR_new_baseline4_noisestd_0.001_1_rollout_0.gif)|0.0009|
|6|2e-3|![animation](./noise_std/3D_AoR_new2_3D_AoR_new_baseline4_noisestd_0.002_1_rollout_0.gif)|0.0007|

### 1.3 Investigation of different edge embedding length

- conclusion: MSE are not good for all of 4 cases, but edge embedding length of 13 has a lower MSE.

|Experimental No.|edge embedding length|animation|MSE|
|:-:|:-:|:-:|:-:|
|1|4|![animation](./edge_in/edgein_4_rollout_0.gif)|0.019|
|2|12|![ani](./edge_in/edgein_12_rollout_0.gif)|0.0397|
|3|13|![ani](./edge_in/edgein_13_rollout_0.gif)|0.0067|
|4|15|![ani](./edge_in/edgein_15_rollout_0.gif)|0.234|

### 1.4 Investigation of different sampling rate
- conclusion: Sampling rate of 20 in this dataset is best among those 4 options

|Experimental No.|sampling rate|animation|MSE|
|:-:|:-:|:-:|:-:|
|1|1|![animation](./sample_freq/rollouts_3D_AoR_new2_3D_AoR_new2_baseline4_samplefreq_1_0_rollout_0.gif)|0.0063|
|2|5|![ani](./sample_freq/rollouts_3D_AoR_new2_3D_AoR_new2_baseline4_samplefreq_5_0_rollout_0.gif)|0.0013|
|3|10|![ani](./sample_freq/rollouts_3D_AoR_new2_3D_AoR_new2_baseline4_samplefreq_10_0_rollout_0.gif)|6.96e-5|
|4|20|![ani](./sample_freq/rollouts_3D_AoR_new2_3D_AoR_new2_baseline4_samplefreq_20_0_rollout_0.gif)|1.37e-5|

## 2. GNS implementation on a new quasi-2D Angle of Repose dataset (500 trajectories)
- sampling rate = 10
- input sequence length = 6
- noise_std = 3e-4
- edge embedding length = 16
- mlp size = 32
- training stops after 18,000 steps

|Loss curve on validation dataset|loss curve on training dataset|
|:---:|:---:|
|![curve1](./3D_AoR_new3_latest/Screenshot_1.png)|![curve2](./3D_AoR_new3_latest/Screenshot_2.png)|


|Experimental No.|animation|MSE|
|:-:|:-:|:-:|
|1|![animation](./3D_AoR_new3_latest/3D_AoR_new3_3D_AoR_new3_baseline4_L1loss_sample10_0_rollout_0.gif)|0.00015|
|2|![animation](./3D_AoR_new3_latest/3D_AoR_new3_3D_AoR_new3_baseline4_L1loss_sample10_0_rollout_1.gif)|0.0002|
|3|![animation](./3D_AoR_new3_latest/3D_AoR_new3_3D_AoR_new3_baseline4_L1loss_sample10_0_rollout_2.gif)|0.00017|
|4|![animation](./3D_AoR_new3_latest/3D_AoR_new3_3D_AoR_new3_baseline4_L1loss_sample10_0_rollout_3.gif)|0.0002|
|5|![animation](./3D_AoR_new3_latest/3D_AoR_new3_3D_AoR_new3_baseline4_L1loss_sample10_0_rollout_4.gif)|0.00018|
