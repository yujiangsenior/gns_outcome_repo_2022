# GNS_outcome after Jan 21


## Jan 22
- baseline_2，MLP神经元个数修改为32 (baseline_2是128)

|lossplot|gif|
|:-:|:-:|
|![mlp32](./baseline_2_mlp32/Screenshot_loss_mlp32.png)|![gif](./baseline_2_mlp32/1000_3region_cohesion_meanstd_adjust_mlp32_0_rollout_3.gif)|

- baseline_2, 修改为nodefeature length =14

|lossplot|gif|
|:-:|:-:|
|![nodefeature14](./baseline_2_nodefeature14/Screenshot_loss_nodefeature14.png)|![gif](./baseline_2_nodefeature14/1000_3region_cohesion_meanstd_adjust_nodefeature14_0_rollout_3.gif)|

- baseline_2, 修改mlp32, nodefeature length =14

|lossplot|gif|
|:-:|:-:|
|![loss](./baseline_2_mlp32_nodefeature14/Screenshot_loss_mlp32_nodefeature14.png)|![gif](./baseline_2_mlp32_nodefeature14/1000_3region_cohesion_meanstd_adjust_mlp32_nodefeature14_0_evalbest_rollout_3.gif)|

- baseline_2, 修改nodefeature length =14, edge12

|lossplot|gif|
|:-:|:-:|
|![loss](./baseline_2_node14edge12/nodefeature14_edge12_2.png)|![gif](./baseline_2_node14edge12/1000_3region_cohesion_meanstd_adjust_nodefeature14_edge12_3_rollout_3.gif)|

- baseline_2, 修改edgeembed length 12

|lossplot|gif|
|:-:|:-:|
|![loss](./baseline_2_edge12/Screenshot_edgefeature12_10.png)|![gif](./baseline_2_edge12/1000_3region_cohesion_meanstd_adjusted_edgeem12_10_rollout_3.gif)|

- baseline_2, 修改edgeembed length 12, input length 3

|lossplot|gif|
|:-:|:-:|
|![loss](./baseline_2_edge12input3/Screenshot_edge12_input3_5.png)|![gif](./baseline_2_edge12input3/bessemer_data_rollouts_meanstd_adjust_edgeembed12_input3_5_rollout_3.gif)|

- baseline_2, 修改edgeembed length 12, nodeembed length 14,  r = 0.004(baseline_2是0.015)

|lossplot|gif|
|:-:|:-:|
|![loss](./baseline_2_node14edge12_r0004/Screenshot_node14edge12_r0004_1.png)|![gif](./baseline_2_node14edge12_r0004/1000_3region_cohesion_meanstd_adjusted_edge12node14_r0004_1_rollout_3.gif)|

- baseline_2, 修改edgeembed length 12, nodeembed length 8, input length 3

|lossplot|gif|
|:-:|:-:|
|![loss](./baseline_2_node8edge12_input3/Screenshot_node8edge12_input3_1.png)|![gif](./baseline_2_node8edge12_input3/bessemer_data_rollouts_meanstd_adjust_edge12node8_input3_1_rollout_3.gif)|