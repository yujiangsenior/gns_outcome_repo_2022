#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 14 17:14:01 2021

@author: yujiang
"""

import pickle
import pprint
import numpy as np
import os

def output_vtk(saveName, idd, PX, PY, PZ, Radius=2.0e-3, Vel=None):
    npar = len(PX)#length of PX
    file = open(saveName,"w")
    file.write('# vtk DataFile Version 2.0\n')
    file.write('Particle data\nASCII\n')
    file.write('DATASET POLYDATA\n')
    file.write('POINTS {} double\n'.format(npar))
    for i in range(npar):
        file.write('{:.8e}\t{:.8e}\t{:.8e}\n'.format(PX[i], PY[i], PZ[i]))###3D case
#        file.write('{:.8e}\t{:.8e}\n'.format(PX[i], PY[i]))###2D case
    file.write( 'VERTICES {} {}\n'.format(npar, npar*2) )
    for i in range(npar):
        file.write('1 {}\n'.format(idd[i]))
    file.write( 'POINT_DATA {}\n'.format(npar))
    file.write( 'SCALARS Radius double 1\n')
    file.write( 'LOOKUP_TABLE default\n')
    for i in range(npar):
        file.write('{:.8e}\n'.format(Radius))
    if Vel is not None:
        file.write( 'VECTORS velocity double\n')
        for i in range(npar):
            file.write('{:.8e}\t{:.8e}\t{:.8e}\n'.format(Vel[0,i],Vel[1,i],Vel[2,i]))
    file.close()

file = open('./rollout_0.pkl', 'rb')
data = pickle.load(file)
pprint.pprint(data)
print(type(data))
#print(type(data['predicted_rollout'])),
#'particle_types', 'metadata', 'initial_positions', 'ground_truth_rollout'
#print(data['predicted_rollout'].shape)
file.close()

dirname = '3Ddrum_edge16node21_3'
#Pid, X_train, t_train, Scale= pre_data(inputFile, nts=nts, nte=nte, ndof=ndof, scale=scale)

steps_t = data['predicted_rollout'].shape[0]#94
num_P = data['predicted_rollout'].shape[1]#904
n_dim = data['predicted_rollout'].shape[2]#3
Radius = 2.0e-3
Pid = np.arange(1, num_P + 1, 1)
os.makedirs(dirname, exist_ok=True)

for i in range(steps_t):
    saveName = (f"./{dirname}/predicted_{i:05d}.vtk")
    PXi = data['predicted_rollout'][i, :, 0]
    PYi = data['predicted_rollout'][i, :, 1]
#    PZi = np.zeros(num_P)
    PZi = data['predicted_rollout'][i, :, 2]###3D case
#    PZi = data['predicted_rollout'][i, :, 1]###2D case
    Vel =None
    output_vtk(saveName, Pid, PXi, PYi, PZi, Radius, Vel)

for i in range(steps_t):
    saveName = (f"./{dirname}/True_{i:05d}.vtk")
    PXi = data['ground_truth_rollout'][i, :, 0]
    PYi = data['ground_truth_rollout'][i, :, 1]
    PZi = data['ground_truth_rollout'][i, :, 2]###3D case
#    PZi = data['ground_truth_rollout'][i, :, 1]###2D case
#    PZi = np.zeros(num_P)
    Vel =None
    output_vtk(saveName, Pid, PXi, PYi, PZi, Radius, Vel)

