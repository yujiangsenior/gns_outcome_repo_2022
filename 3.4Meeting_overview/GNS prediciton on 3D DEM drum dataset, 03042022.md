# Recent work for GNS prediction and literature review, 03/04/2022

## 1.One experiment for the 3D DEM drum dataset
- connectivity radius = 0.025
- message passing steps = 12
- noise_std = 3e-4
- mean and std of velocity, acceleration is calculated based on the given trajectories, so velocity and acceleration is normalized during training and infering
- Edge embedding length = 16
- Node embedding length = 21
|Training trajectories used|Ground Truth vs Prediction|
|:--:|:--:|
|24|![animation](./3D_drum_new_baseline3_0/GIF 2022-3-4 5-43-04.gif)|
|6|![animation](./3D_drum_baseline3_3/GIF 2022-3-4 5-39-00.gif)|

## 2.Setting up a DEM simulation for quasi-2D Angle of Repose in MFiX
![geometry of AoR](./Screenshot_2.png)

## 3.Literature review

|Literature No|Title|Approach|Main contribution|
|--|--|--|--|
|1|Learning Physics-Consistent Particle Interactions|Graph Neural Network(GNN)|Node part of the GNN is designed that restricts the output space of edge part of GNN, so that the results align with Newton's laws|
|2|Physical Design using Differentiable Learned Simulators|GNN, gradient descent based optimization|Solve the inverse design problems by a faster, general-purpose way|
|3|E(n) Equivariant Graph Neural Networks|GNN|GNN equivariant to rotations, translations, reflections and permutations|

